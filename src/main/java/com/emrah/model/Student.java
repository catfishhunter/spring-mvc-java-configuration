package com.emrah.model;

import java.util.LinkedHashMap;

import lombok.Data;

@Data
public class Student {

	private String firstName;
	private String lastName;
	private String country;
	private String state;
	private String team;
	private String favoriteLanguage;
	private String[] operatingSystems;
	
	private LinkedHashMap<String, String> states;
	
	
	public Student() {
		states = new LinkedHashMap<String, String>();
		states.put("LA", "Los Angeles");
		states.put("CA", "California");
		states.put("NY", "New York");
		states.put("PH", "Phoneix");
	}
	
	
}
